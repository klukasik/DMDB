CREATE VIEW Male AS
SELECT * FROM Students
WHERE gender=True;

DROP VIEW Male;

CREATE TABLE Male AS
SELECT * FROM Students
WHERE gender=True;

CREATE TABLE Female AS
SELECT * FROM Students
WHERE gender=False;

/*
updatable views:
- operation only modifies one base table at a time
- no aggregate functions used
- no derived columns
*/

CREATE VIEW Twenty AS
SELECT * FROM
(
	SELECT * FROM Male
	UNION
	SELECT * FROM Female
)
WHERE age=20;

/*
in psql you can alter the view columns etc
*/
