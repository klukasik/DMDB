DROP VIEW v1;
DROP VIEW v2;
DROP VIEW v3;
DROP VIEW v4;
DROP VIEW v5;
DROP VIEW v6;
DROP VIEW v7;
DROP VIEW v8;
DROP VIEW v9;
DROP VIEW v10;


-- WHERE
CREATE VIEW v1 AS
SELECT *
FROM Students
WHERE age=20;

-- ORDER BY
CREATE VIEW v2 AS
SELECT *
FROM Students
ORDER BY name;

-- DISTINCT
CREATE VIEW v3 AS
SELECT DISTINCT age
FROM Students;

-- DISTINCT by GROUP BY
CREATE VIEW v4 AS
SELECT MAX (name) name 
FROM
(
	SELECT *
	FROM Students
	ORDER BY name
)
GROUP BY age;

-- 3 youngest students
CREATE VIEW v5 AS
SELECT *
FROM Students
ORDER BY age
LIMIT 3;

-- min age
CREATE VIEW v6 AS
SELECT MIN(age)
FROM Students;

-- avg female age
CREATE VIEW v7 AS
SELECT AVG(age)
FROM Students
GROUP BY gender;

-- count students
CREATE VIEW v8 AS
SELECT COUNT(id)
FROM Students;

-- start letters Ja
CREATE VIEW v9 AS
SELECT *
FROM Students
WHERE name LIKE 'Ja%';

-- start letters Ja
-- substring (attr, start, legnth), where first letter indexed 1
CREATE VIEW v10 AS
SELECT *
FROM Students
WHERE substring(name, 1, 2) = 'Ja';
