\connect postgres

CREATE TABLE Students (
	name 	VARCHAR(20) NOT NULL,
	id 	INT,
	age	INT,
	UNIQUE (id)
);

CREATE TABLE Blacklist (
	id INT,
	UNIQUE (id)
);

