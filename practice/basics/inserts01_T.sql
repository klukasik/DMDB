-- Template where PATH can be easily replaced
\COPY Students FROM 'PATH/practice/basics/data/students.tbl' WITH delimiter AS '|';
\COPY Blacklist FROM 'PATH/practice/basics/data/blacklist.tbl' WITH delimiter AS '|';
