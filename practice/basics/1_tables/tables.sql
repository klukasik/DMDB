/*
using scheme01.sql

practice: tables
*/


-- ##########
--COPY TABLE


-- V 1
SELECT *
INTO StudentsC1
FROM Students;

-- V 2
CREATE TABLE StudentsC2 AS
SELECT * 
FROM Students;

-- V 3
CREATE TABLE StudentsC3 AS
SELECT * FROM Students LIMIT 0;

INSERT INTO StudentsC3
SELECT *
FROM Students;


-- ##########
-- DROP TABLE

SELECT INTO StudentsC11
FROM Students;

DROP TABLE StudentsC11;


-- ##########
-- UPDATE
UPDATE Students
SET name='Barack'
WHERE id=1;

UPDATE Students
SET age = age + 1;

-- ##########
-- ALTER TABLE

ALTER TABLE Students
ADD gender BOOLEAN;
