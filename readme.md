# DMDB
## Info about Repository
- for each Quiz separate folder
- separate folders for separate schemas,
or if large schmeas: separate folders for separate tables
- special.sql has practice examples
- inserts.sql inserts data into the tables
- inserts_T.sql is a template, where 'PATH' can be easily replaced



<!-- Practice Tools -->

## Practice Tools
- <https://sqlfiddle.com/>
- <https://www.tutorialspoint.com/online_sql_editor.htm>
- DOCKER <br>



<!-- Docker & PostgreSQL -->

# Docker & PostgreSQL

#### Tutorials
- <https://dev.to/andre347/how-to-easily-create-a-postgres-database-in-docker-4moj>
- <https://www.baeldung.com/ops/docker-databases>
- <https://www.sqltutorial.net/>



<!-- Docker -->

## Docker
used for running, developing, shipping applications

#### start docker deamon
systemctl start docker.service

#### create container
docker run --name postgres-db -e POSTGRES_PASSWORD=docker -p 5432:5432 -d postgres

name: postgres-db <br>
username: postgres <br>
password: docker <br>
port: 5432 <br>

#### start container/server
docker start "server name"

#### login to server
docker ps 
(show containers)

docker exec -it postgres-db bash 
(connect to db via bash)

#### File Management
2 Options to copy files onto the server: <br>
- docker cp SOURCE container:/DEST <br>
- git works perfectly



<!-- PostgreSQL -->

## PostgreSQL
The server in Docker Container <br>
- postgres: object-relational database management system (i.e. database server)

psql -U postgres 
(connect to database to run SQL)

psql -U username -f filepath (executes sql file on server)



### installed
uses apt package manager

neovim



### PSQL (postgreSQL)
\dt
(list talbes)

\! clear 
(clears screen)

ctl + l 
(clears screen)

### SQL

```sql
SELECT * FROM table;

CREATE TABLE tableName (attribute TYPE, ...);

INSERT INTO table (attr1, attr2, ..) VALUES (val1, val2, ..);
```



