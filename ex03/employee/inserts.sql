﻿-- IMPORTANT: Replace PATH_TO_DATASET with the path to the Employee dataset

COPY employees FROM 'PATH_TO_DATASET/data/employees.tbl' WITH delimiter AS '|';
COPY departments FROM 'PATH_TO_DATASET/data/departments.tbl' WITH delimiter AS '|';
COPY dept_manager FROM 'PATH_TO_DATASET/data/dept_manager.tbl' WITH delimiter AS '|';
COPY dept_emp FROM 'PATH_TO_DATASET/data/dept_emp.tbl' WITH delimiter AS '|';
COPY titles FROM 'PATH_TO_DATASET/data/titles.tbl' WITH delimiter AS '|';
COPY salaries FROM 'PATH_TO_DATASET/data/salaries.tbl' WITH delimiter AS '|';
