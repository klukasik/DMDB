
CREATE TABLE stops(
	stop_id bigint PRIMARY KEY,
	stop_name varchar(40),
	stop_lat float,
	stop_lon float);

CREATE TABLE trips(
	trip_id bigint PRIMARY KEY,
	tram_number integer,
	trip_headsign varchar(30));

CREATE TABLE stop_times(
	trip_id bigint REFERENCES trips,
	arrival_time varchar(9),
	departure_time varchar(9),
	stop_id bigint REFERENCES stops,
	stop_sequence integer,
	PRIMARY KEY (trip_id, stop_sequence));

