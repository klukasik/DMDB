CREATE TABLE Coach (
    name STRING,
    email STRING,
    PRIMARY KEY (name)
);

CREATE TABLE Player (
    name STRING,
    score INT,
    numberAttr INT,
    PRIMARY KEY (name)
);

CREATE TABLE Team (
    sub_id INT,
    name STRING,
    PRIMARY KEY (sub_id)
);

CREATE TABLE coaching (
    name STRING UNIQUE,
    sub_id INT UNIQUE,
    CONSTRAINT cname
    FOREIGN KEY (name) REFERENCES Coach (name)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
    CONSTRAINT csub,
    FOREIGN KEY (sub_id) REFERENCES Team (sub_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);

CREATE TABLE game (
    sub_idA INT,
    sub_idB INT,
    scoreA INT,
    scoreB INT,
    CONSTRAINT csub1
    FOREIGN KEY (sub_idA) REFERENCES Team (sub_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
    CONSTRAINT csub2
    FOREIGN KEY (sub_idB) REFERENCES Team (sub_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);

CREATE TABLE captain (
    name STRING UNIQUE,
    sub_id INT UNIQUE,
    CONSTRAINT cname
    FOREIGN KEY (name) REFERENCES Player (name)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
    CONSTRAINT csub
    FOREIGN KEY (sub_id) REFERENCES Team (sub_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);

CREATE TABLE playing (
    name STRING UNIQUE,
    sub_id INT,
    CONSTRAINT cname
    FOREIGN KEY (name) REFERENCES Player (name)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
    CONSTRAINT csub
    FOREIGN KEY (sub_id) REFERENCES Team (sub_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);





