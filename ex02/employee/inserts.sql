﻿\c ex02_1
\COPY employees FROM '/root/git/DMDB/ex02_2/employee/data/employees.tbl' WITH delimiter AS '|';
\COPY departments FROM '/root/git/DMDB/ex02_2/employee/data/departments.tbl' WITH delimiter AS '|';
\COPY dept_manager FROM '/root/git/DMDB/ex02_2/employee/data/dept_manager.tbl' WITH delimiter AS '|';
\COPY dept_emp FROM '/root/git/DMDB/ex02_2/employee/data/dept_emp.tbl' WITH delimiter AS '|';
\COPY titles FROM '/root/git/DMDB/ex02_2/employee/data/titles.tbl' WITH delimiter AS '|';
\COPY salaries FROM '/root/git/DMDB/ex02_2/employee/data/salaries.tbl' WITH delimiter AS '|';
