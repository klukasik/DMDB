\c ex02_1

SELECT CAST(min(s.salary) as float)/max(s.salary) AS ratio, de.from_date AS year
FROM departments d
JOIN dept_emp de USING(dept_no)
JOIN salaries s USING(emp_no)
WHERE d.dept_name = 'Marketing'
GROUP BY de.from_date;
