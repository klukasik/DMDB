
/*SELECT COUNT(*)
FROM customer c 
JOIN nation n USING (nationid)
WHERE n.nationname = 'JAPAN';

SELECT * 
FROM ( 	
	SELECT *
	FROM supplypart
	JOIN supplier USING (supplierid)
	WHERE suppliername = 'Supplier#000000001'
) s1
LEFT JOIN ( 
	SELECT *
	FROM supplypart
	JOIN supplier USING (supplierid)
	WHERE suppliername = 'Supplier#000000002'
) s2
USING (partid)
WHERE s2.suppliername IS NULL;
*/

SELECT COUNT (*)
FROM (
SELECT COUNT (*)
FROM customer c
JOIN orders USING (customerid)
JOIN orderline ol USING (orderid)
WHERE 
(
	SELECT COUNT (*)
	FROM supplier s
	JOIN nation USING (nationid)
	WHERE c.nationid = s.nationid
	AND ol.supplierid = s.supplierid
) >= 1
GROUP BY c.customerid
);
