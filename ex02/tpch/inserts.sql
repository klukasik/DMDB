﻿\c ex02_3
\COPY REGION FROM '/root/git/DMDB/ex02/tpch/data/region.tbl' WITH DELIMITER AS '|';
\COPY NATION FROM '/root/git/DMDB/ex02/tpch/data/nation.tbl' WITH DELIMITER AS '|';
\COPY PART FROM '/root/git/DMDB/ex02/tpch/data/part.tbl' WITH DELIMITER AS '|';
\COPY SUPPLIER FROM '/root/git/DMDB/ex02/tpch/data/supplier.tbl' WITH DELIMITER AS '|';
\COPY SUPPLYPART FROM '/root/git/DMDB/ex02/tpch/data/supplypart.tbl' WITH DELIMITER AS '|';
\COPY CUSTOMER FROM '/root/git/DMDB/ex02/tpch/data/customer.tbl' WITH DELIMITER AS '|';
\COPY ORDERS FROM '/root/git/DMDB/ex02/tpch/data/orders.tbl' WITH DELIMITER AS '|';
\COPY ORDERLINE FROM '/root/git/DMDB/ex02/tpch/data/orderline.tbl' WITH DELIMITER AS '|';

