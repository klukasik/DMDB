\c ex02_2

SELECT * 
FROM stops
WHERE stop_name LIKE '%ETH%';

SELECT *
FROM stops
WHERE stop_name LIKE '%Milchbuck';



SELECT *
FROM stop_times
WHERE CAST (substring(departure_time, 1, 2) AS INT) BETWEEN 13 AND 14
AND CAST (substring(arrival_time, 1, 2) AS INT) BETWEEN 13 AND 13
ORDER BY departure_time ASC;
