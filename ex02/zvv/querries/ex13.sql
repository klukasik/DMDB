\c ex02_2

-- find out max sequence number for each trip
-- max sequence number -> endstation of trip
-- check whether Milchbuck is endstation
/*
WITH maxSeq AS (
	SELECT MAX(stop_sequence) stop_sequence, trip_id
	FROM trips 
	JOIN stop_times USING (trip_id)
	GROUP BY trip_id
)
SELECT *
FROM maxSeq
JOIN stop_times USING (trip_id, stop_sequence)
JOIN stops USING (stop_id)
WHERE stop_name LIKE '%Milchbuck%';

-- gather all data (stops x trips x stop_times) with sequence 1
-- and that are at stop ETH, i.e. data about trips that depart from eth
WITH minSeq AS (
	SELECT *
	FROM trips
	JOIN stop_times USING(trip_id)
	WHERE stop_sequence = 1
)
SELECT *
FROM minSeq
JOIN stops USING (stop_id)
WHERE stop_name LIKE '%ETH%';
*/





/*

sadly, the task is shitly written. Still guessing what that task wants, but lets try something else

*/

WITH R1 AS (
	SELECT *
	FROM trips
	JOIN stop_times USING (trip_id)
	JOIN stops USING (stop_id)
)
SELECT R2.stop_name AS departing_station, R3.stop_name AS arrival_station, R2.tram_number AS tram_number, R2.departure_time as departure_time, R3.arrival_time as arrival_time, R2.trip_headsign as trip_headign
FROM (
	SELECT *
	FROM R1
	WHERE stop_name LIKE '%ETH%'
) R2
JOIN (
	SELECT *
	FROM R1
	WHERE stop_name LIKE '%Milchbuck%'
) R3 USING (trip_id)
WHERE R2.stop_sequence < R3.stop_sequence
AND CAST (substring(R2.departure_time, 1, 2) AS INT) = 13
AND CAST (substring(R3.arrival_time, 1, 2) AS INT) = 13
ORDER BY R2.departure_time;

