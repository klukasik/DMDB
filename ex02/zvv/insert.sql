﻿\c ex02_2
\COPY stops FROM '/root/git/DMDB/ex02_2/zvv/data/stops.tbl' WITH delimiter AS '|';
\COPY trips FROM '/root/git/DMDB/ex02_2/zvv/data/trips.tbl' WITH delimiter AS '|';
\COPY stop_times FROM '/root/git/DMDB/ex02_2/zvv/data/stop_times.tbl' WITH delimiter AS '|';
