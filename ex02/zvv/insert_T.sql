﻿-- IMPORTANT: Replace PATH_TO_DATASET with the path to the ZVV dataset
\c ex02_2
\COPY stops FROM 'PATH_TO_DATASET/data/stops.tbl' WITH delimiter AS '|';
\COPY trips FROM 'PATH_TO_DATASET/data/trips.tbl' WITH delimiter AS '|';
\COPY stop_times FROM 'PATH_TO_DATASET/data/stop_times.tbl' WITH delimiter AS '|';
