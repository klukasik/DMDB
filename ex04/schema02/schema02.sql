-- used for Question 3, 8, 9

CREATE TABLE ipv4addr (

  ipv4 VARCHAR(15) PRIMARY KEY,

  next_hop VARCHAR(15)

);

CREATE TABLE ipv6addr (

  ipv6 VARCHAR(45) PRIMARY KEY,

  next_hop VARCHAR(45)

);

CREATE TABLE nic (

  mac_addr VARCHAR(17) PRIMARY KEY,

  location INT

);

CREATE TABLE device (

  device_id SERIAL,

  device_name VARCHAR(40)

);

CREATE TABLE device_nic (

  mac_addr VARCHAR(17),

  device_id INT,

  PRIMARY KEY (mac_addr, device_id)

);

CREATE TABLE flow (

  source_ip VARCHAR(45),

  dest_ip VARCHAR(45),

  source_port INT,

  dest_port INT,

  proto INT,

  flow_label INT UNIQUE,

  PRIMARY KEY (source_ip, source_port, dest_ip, dest_port, proto)

);
