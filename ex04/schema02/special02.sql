-- delete views, to recreate them
DROP VIEW t1;
DROP VIEW t2;
DROP VIEW t3;
DROP TABLE t4;
DROP TABLE t5;
DROP TABLE t6;
DROP VIEW t6_1;
DROP VIEW t6_2;

-- UNION
-- one attribute in common, 
-- another attribute same name, but different type
CREATE VIEW t1 AS (
	SELECT * 
	FROM ipv4addr
	
	UNION

	SELECT *
	FROM ipv6addr
);

-- UNION ALL
-- one attribute in common,
-- another attribute same name, but different type
CREATE VIEW t2 AS (
	SELECT * 
	FROM ipv4addr
	
	UNION ALL

	SELECT *
	FROM ipv6addr
);

-- INNER JOIN ... ON
CREATE VIEW t3 AS (
	SELECT a.ipv4 as attr1, b.ipv4 as attr2
	FROM ipv4addr a

	INNER JOIN

	ipv4addr b
	ON a.next_hop = b.next_hop
);


