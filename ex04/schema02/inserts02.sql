-- Template where /root/git/DMDB can be easily replaced
\COPY ipv4addr FROM '/root/git/DMDB/ex04/schema02/data/ipv4addr.tbl' WITH delimiter AS '|';
\COPY ipv6addr FROM '/root/git/DMDB/ex04/schema02/data/ipv6addr.tbl' WITH delimiter AS '|';
