-- Template where PATH can be easily replaced
\COPY Artist FROM 'PATH/ex04/schema01/data/artist.tbl' WITH delimiter AS '|';
\COPY Releases FROM 'PATH/ex04/schema01/data/releases.tbl' WITH delimiter AS '|';
\COPY ReleasedBy FROM 'PATH/ex04/schema01/data/releasedby.tbl' WITH delimiter AS '|';
\COPY Track FROM 'PATH/ex04/schema01/data/track.tbl' WITH delimiter AS '|';
\COPY Genre FROM 'PATH/ex04/schema01/data/genre.tbl' WITH delimiter AS '|';
\COPY Users FROM 'PATH/ex04/schema01/data/users.tbl' WITH delimiter AS '|';
\COPY UserLikesArtist FROM 'PATH/ex04/schema01/data/userlikesartist.tbl' WITH delimiter AS '|';
\COPY UserLikesTrack FROM 'PATH/ex04/schema01/data/userlikestrack.tbl' WITH delimiter AS '|';

