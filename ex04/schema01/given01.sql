CREATE VIEW q1 AS (
	SELECT a.artistId, COUNT(*)
	FROM Artist a, ReleasedBy rb, Releases r, Track t

	WHERE a.artistId = rb.artistId
		AND rb.releaseId = r.releaseId
		AND r.releaseId = t.releaseId

	GROUP BY a.artistId
 	ORDER BY COUNT(*) DESC
);

CREATE VIEW q2 AS (
	SELECT sub1.artistId1, sub1.c1+sub2.c2
	FROM (
		SELECT ua.artistId AS artistId1, COUNT(*) AS c1
		FROM UserLikesArtist ua
		GROUP BY ua.artistId
    	) AS sub1,

    	(
		SELECT a.artistId AS artistId2, COUNT(*) AS c2
		FROM UserLikesTrack ut, ReleasedBy rb, Artist a
		WHERE a.artistId = rb.artistId
			AND rb.releaseId = ut.releaseId
		GROUP BY a.artistId
    	) AS sub2

	WHERE sub1.artistId1=sub2.artistId2
	ORDER BY sub1.c1+sub2.c2 DESC
	LIMIT 1
);

CREATE VIEW q3 AS (
	SELECT a.artistId, COUNT(t.trackName)
	FROM Artist a, ReleasedBy rb, Releases r, Track t

	WHERE a.artistId = rb.artistId
	    AND rb.releaseId = r.releaseId
	    AND r.releaseId = t.releaseId

	GROUP BY a.artistId
	ORDER BY COUNT(t.trackName) DESC
);

CREATE VIEW q4 AS (
	SELECT column1, column2
	FROM (
		SELECT a.artistId AS column1, COUNT(*) AS column2
		FROM Artist a, UserLikesArtist ua, Users u
		WHERE a.artistId = ua.artistId AND u.userId = ua.userId
		GROUP BY a.artistId
	) AS sub1

	UNION

	SELECT column1, column2
	FROM (
		SELECT a.artistId as column1, 0 as column2
		FROM Artist a
		WHERE a.artistId NOT IN (SELECT artistID FROM UserLikesArtist)
	) AS sub2

	WHERE column2 < 5
	ORDER BY column2 ASC
);

CREATE VIEW q5 AS (
	SELECT sub.artistId, COUNT(*)
	FROM (
		SELECT ua.artistId, ua.userId
		FROM UserLikesArtist ua
		
		UNION
		
		SELECT a.artistId, u.userId
		FROM Users u, UserLikesTrack ut, ReleasedBy rb, Artist a
		WHERE a.artistId = rb.artistId
		    AND rb.releaseId = ut.releaseId
		    AND ut.userId = u.userId
    	) AS sub

	GROUP BY sub.artistId
	ORDER BY COUNT(*) DESC
	LIMIT 1
);

CREATE VIEW q6 AS (
	SELECT column1, column2
 	FROM (
		SELECT a.artistId AS column1, COUNT(*) AS column2
		FROM Artist a, UserLikesArtist ua, Users u
		WHERE a.artistId = ua.artistId AND u.userId = ua.userId
		GROUP BY a.artistId
    	) AS sub1

	UNION

	SELECT column1, column2
	FROM (
		SELECT a.artistId as column1, 0 as column2
		FROM Artist a
		WHERE a.artistId NOT IN (SELECT artistID FROM UserLikesArtist ul, Track t WHERE ul.artistId = t.artistId)
	) AS sub2

	WHERE column2 < 5
	ORDER BY column2 ASC
);
