/*
special type of querries
 */

-- drop views, to redo them
DROP VIEW t1;
DROP VIEW t2;

-- LIMIT function
CREATE VIEW t1 AS (
	SELECT *
	FROM Artist
	LIMIT 1
);

-- SELECT 0 ?
-- this returns one column, filled with zeros for all tuples that were in the selection
-- if some comparison (WHERE condition) would be made on a NULL state entry, then that tuple would not be taken, hence there would be no 0 in the column for that tuple
CREATE VIEW t2 AS (
	SELECT 0
	FROM Artist
);
