-- Scheme for Questions 1, 2

CREATE TABLE Artist (
	artistId INT,
	artistName VARCHAR(20),
	artistCountry VARCHAR(20),
	UNIQUE (artistId)
);

CREATE TABLE Releases (
	releaseId INT,
	releaseName VARCHAR(20),
	releaseDate DATE,
	genreName VARCHAR(20),
	UNIQUE (releaseId)
);

CREATE TABLE ReleasedBy (
	artistId INT, 
	releaseId INT,
	UNIQUE (artistId, releaseId)
);

CREATE TABLE Track (
	releaseId INT,
	trackNumber INT,
	trackName VARCHAR(20),
	genreName VARCHAR(20),
	UNIQUE (releaseId, trackNumber)
);

CREATE TABLE Genre (
	genreName VARCHAR(20),
	parentGenreName VARCHAR(20),
	UNIQUE (genreName)
);

CREATE TABLE Users (
	userId INT,
	userName VARCHAR(20),
	UNIQUE (userId)
);

CREATE TABLE UserLikesArtist (
	userId INT,
	artistId INT,
	UNIQUE (userId, artistId)
);

CREATE TABLE UserLikesTrack (
	userId INT,
	releaseId INT,
	trackNumber INT
);

