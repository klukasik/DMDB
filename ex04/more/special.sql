DROP TABLE t1;
DROP TABLE t2;
DROP VIEW t3_1;
DROP VIEW t3_2;
DROP TABLE t3;
DROP TABLE t4;
DROP TABLE t4__1;
DROP TABLE t4__2;

-- SERIAL type
CREATE TABLE t1 (
	id SERIAL PRIMARY KEY,
	name VARCHAR(20)
);
INSERT INTO t1(name)
VALUES 
	('LeBron'), 
	('Stephen');
-- try inserting NULL for id, NOT possible

-- PRIMARY KEY NULL
CREATE TABLE t2 (
	id INT PRIMARY KEY,
	name VARCHAR(20)
);
-- try inserting NULL for id, NOT possible



-- NULLs COUNT
-- COUNT(attr) ingores NULLs
-- COUNT(*) counts NULLs, yes even full row nulls
CREATE TABLE t3 (
	id INT,
	nameNr INT
);
INSERT INTO t3
VALUES
	(1, 1),
	(2, 2),
	(3, 3),
	(4, NULL),
	(5, NULL),
	(NULL, NULL);
CREATE VIEW t3_1 AS (
	SELECT COUNT(nameNr)
	FROM t3
);
CREATE VIEW t3_2 AS (
	SELECT COUNT(*)
	FROM 
	(SELECT * FROM t3)
);



-- PARTITION BY
/*
PARTITION BY is used by "window functions", that calculate e.g. the average of some rows.
GROUP BY will compress the rows that are being used to calculate a value,
where as PARTITION BY will copy the value for each row
*/

-- UNUPDATABLE VIEWS
/*
VIEWS in SQL are updatable iff:
- involves only one base relation
- involves key of the base relation
- doesn't involve aggregates, group by, or dupplicate elimination

This tries to maintain one-to-one mapping to base relation
*/

-- JOIN BY vs FROM r1, r2 WHERE
/*
JOIN BY will merge the columns, 
where "from r1, r2 where" will show both columns
*/



-- DATE
CREATE TABLE t4 (
	name VARCHAR(20) PRIMARY KEY,
	birthday DATE
);
INSERT INTO t4 
VALUES
	('Alice', NOW()),
	('Bobby', '2000-01-01'),
	('Cedric', '2010-01-01');

-- copy t4
SELECT *
INTO t4__1
FROM t4;

-- subtract nr -> subtract days
UPDATE t4__1
SET birthday = birthday-10;

-- copy t4
SELECT *
INTO t4__2
FROM t4;

-- subtract INTERVAL
UPDATE t4__2
SET birthday = birthday - INTERVAL '10 months';

UPDATE t4__2
SET birthday = birthday - INTERVAL '2 years';

